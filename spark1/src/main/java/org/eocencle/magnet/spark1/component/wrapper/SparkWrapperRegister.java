package org.eocencle.magnet.spark1.component.wrapper;

import org.eocencle.magnet.core.component.wrapper.WrapperManager;
import org.eocencle.magnet.core.component.wrapper.WrapperRegister;
import org.eocencle.magnet.core.util.CoreTag;

/**
 * Spark包装类注册类
 * @author: huan
 * @Date: 2020-04-18
 * @Description:
 */
public class SparkWrapperRegister implements WrapperRegister {
    @Override
    public void register() {
        WrapperManager.registerComponentWrapper(CoreTag.STRING_STAR, SparkSQLTableRegisterWrapper.class);
    }
}
