# Magnet

#### 介绍
一款简单实用的分布式大数据处理框架，特点是零基础操作，支持批处理和流式处理。
<a href='https://gitee.com/huanStephen/magnet/stargazers'><img src='https://gitee.com/huanStephen/magnet/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/huanStephen/magnet/members'><img src='https://gitee.com/huanStephen/magnet/badge/fork.svg?theme=dark' alt='fork'></img></a>
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=06ecb7c18b0f2eec1114a35694602caf690b4ad5b071f83a47bd902162e84ef1"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="创玩大数据" title="创玩大数据"></a>
#### 软件架构
项目目前由core、xmlbuilder、spark1和client四个模块组成，core模块封装了各个部分的抽象组件；xmlbuilder模块是以xml解析为执行配置来源的读取模块；spark1模块为大数据引擎模块；client模块是客户端调用模块。项目可以扩展执行配置的解析方式和大数据处理引擎，良好的接口可以兼容任何大数据引擎。

#### 使用说明
- [Magnet介绍](https://gitee.com/huanStephen/magnet/wikis/pages)
- [快速入门](https://gitee.com/huanStephen/magnet/wikis/pages)
- [配置文件结构](https://gitee.com/huanStephen/magnet/wikis/pages)
- [parameter参数标签](https://gitee.com/huanStephen/magnet/wikis/pages)
- [fragment碎片标签](https://gitee.com/huanStephen/magnet/wikis/pages)
- [datasource数据源标签](https://gitee.com/huanStephen/magnet/wikis/pages)
- [workflow工作流标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [SQL标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [filter标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [distinct标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [output标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [valueMappers标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [splitFieldToRows标签](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [stringCuts标签](https://gitee.com/huanStephen/magnet/wikis/pages)
- [开发指南](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [框架结构介绍](https://gitee.com/huanStephen/magnet/wikis/pages)
> - [扩展新处理引擎](https://gitee.com/huanStephen/magnet/wikis/pages)

#### 参与贡献
- huanStephen
- zhangshk_

#### 项目演示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0331/214445_a5d64ad6_612479.gif "magnet-03311.gif")