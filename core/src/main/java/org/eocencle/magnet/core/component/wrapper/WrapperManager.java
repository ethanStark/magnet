package org.eocencle.magnet.core.component.wrapper;

import org.eocencle.magnet.core.component.WorkStageComponent;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.SQLInfo;
import org.eocencle.magnet.core.mapping.WorkStageInfo;
import org.eocencle.magnet.core.util.CoreTag;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 包装管理类
 * @author: huan
 * @Date: 2020-02-24
 * @Description:
 */
public class WrapperManager {
    // 执行包装map
    private static List<MatchWrapper> runningWrappers = new ArrayList<>();

    static {
        // 收集结果
        registerComponentWrapper(CoreTag.STRING_STAR, CollectResultWrapper.class);
    }

    /**
     * 组件包装
     * @Author huan
     * @Date 2020-02-27
     * @Param [component, info, handler]
     * @Return org.eocencle.magnet.client.component.WorkStageComponent
     * @Exception
     * @Description
     **/
    public static WorkStageComponent wrapper(WorkStageComponent component, WorkStageInfo info, WorkStageHandler handler) {
        component.initData(info);
        component.initHandler(handler);

        try {
            Constructor constructor = null;
            for (MatchWrapper mapper: runningWrappers) {
                if (check(mapper.getMatcher(), info)) {
                    constructor = mapper.getWrapperCls().getConstructor(WorkStageComponent.class);
                    component = (WorkStageComponent) constructor.newInstance(component);
                    component.initData(info);
                    component.initHandler(handler);
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return component;
    }

    /**
     * 添加包装
     * @Author huan
     * @Date 2020-04-18
     * @Param [matcher, wrapperCls]
     * @Return void
     * @Exception
     * @Description
     **/
    public static void registerComponentWrapper(String matcher, Class<? extends WorkStageComponentWrapper> wrapperCls) {
        registerComponentWrapper(new MatchWrapper(matcher, wrapperCls));
    }

    /**
     * 添加包装
     * @Author huan
     * @Date 2020-04-19
     * @Param [matchWrapper]
     * @Return void
     * @Exception
     * @Description
     **/
    public static void registerComponentWrapper(MatchWrapper matchWrapper) {
        runningWrappers.add(matchWrapper);
    }

    /**
     * 验证是否添加包装
     * @Author huan
     * @Date 2020-04-18
     * @Param [matcher, info]
     * @Return boolean
     * @Exception
     * @Description
     **/
    private static boolean check(String matcher, WorkStageInfo info) {
        if (CoreTag.STRING_STAR.equals(matcher)) {
            return true;
        } else if (CoreTag.XML_EL_SQL.equals(matcher) && info instanceof SQLInfo) {
            return true;
        }
        return false;
    }
}
