package org.eocencle.magnet.core.component.wrapper;

/**
 * 匹配的包装类
 * @author: huan
 * @Date: 2020-04-19
 * @Description:
 */
public class MatchWrapper {
    // 匹配值
    private String matcher;
    // 包装类
    private Class<? extends WorkStageComponentWrapper> wrapperCls;

    public MatchWrapper() {
    }

    public MatchWrapper(String matcher, Class<? extends WorkStageComponentWrapper> wrapperCls) {
        this.matcher = matcher;
        this.wrapperCls = wrapperCls;
    }

    public String getMatcher() {
        return matcher;
    }

    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    public Class<? extends WorkStageComponentWrapper> getWrapperCls() {
        return wrapperCls;
    }

    public void setWrapperCls(Class<? extends WorkStageComponentWrapper> wrapperCls) {
        this.wrapperCls = wrapperCls;
    }
}
